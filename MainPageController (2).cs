using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DEL2.Models;
using System.Data.SqlClient;
using DEL2.Controllers;
using System.Security.Cryptography;
using System.Text;
using DEL2.ViewModels;

namespace DEL2.Controllers
{
    public class MainPageController : Controller
    {
        Doctor24Entities2 db = new Doctor24Entities2();
        // GET: MainPage
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel login)
        {
            if (ModelState.IsValid)
            {
                Doctor24Entities2 db = new Doctor24Entities2();
                var user = (from userlist in db.USERs
                            where userlist.Email == login.UserName && userlist.Password == login.Password
                            select new
                            { userlist.UserID, userlist.Email }
                               ).ToList();

                if (user.FirstOrDefault() != null)
                {
                    Session["UserName"] = user.FirstOrDefault().Email;
                    Session["UserID"] = user.FirstOrDefault().UserID;
                    return Redirect("DOCTOR");
                }
                else
                {
                    ModelState.AddModelError("", "Invalid login credentials.");
                }

            }
            return View("login");
        }


        public ActionResult HOME()
        {
            //  Session["UserID"] = null;
            return View();
        }
        public ActionResult UserLogin()
        {

            return View("");
        }

        [HttpPost]

        //public ActionResult UserLogin(string email, string password)
        //{
        //    var hashedPassword = ComputeSha256Hash(password);
        //    Models.USER user = db.USERs.Where(zz => zz.Email == email && zz.Password == hashedPassword).FirstOrDefault();

        //    if (user != null)
        //    {
        //        Session["UserID"] = user.UserID;
        //        Session["UserName"] = user.Email;

        //        return RedirectToAction("UserDashBoard");
        //    }
        //    else
        //    {
        //        ViewBag.Message = "Please enter valid username or password";
        //        return View("HOME"); //Add error message
        //    }
        //}


        //public ActionResult UserDashboard()
        //{
        //    if (Session["UserID"] != null)
        //    {
        //        ViewBag.Message = "Welcome" + Session["UserName"];
        //        return View("PatRegistration");
        //    }
        //    else
        //    {
        //        return RedirectToAction("HOME");
        //    }
        //}


        //if(user != null)
        //{
        //    return RedirectToAction("DOCTOR");
        //}
        //else
        //{
        //    return RedirectToAction("");
        //}

        //RENDANI
        //if (user != null)
        //{
        //    UserVM userVME = new UserVM();
        //    userVME.user = user;
        //    userVME.RefreshGUID(db);
        //    TempData["UserVM"] = userVME;
        //    return RedirectToAction("HOME", "Customer");
        //}

        //return RedirectToAction("Data", "DocRegistration");

        [HttpGet]
        public ActionResult DOCTOR()
        {
            return View(db.Appointments.ToList());
        }
        [HttpGet]
        public ActionResult APPOINTMENT()
        {
            List<Appointment> appointments = new List<Appointment>();
            return View(db.DOCTORs.ToList());


        }
        public ActionResult PatientsAwaitingConf()
        {
            List<PATIENT> patientss = new List<PATIENT>();
            return View();
        }


        public ActionResult Logout()
        {
            Session["UserID"] = null;
            return RedirectToAction("HOME");
        }

        //string ComputeSha256Hash(string rawData)
        //{
        //    using (SHA256 sha256Hash = SHA256.Create())
        //    {
        //        byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

        //        StringBuilder builder = new StringBuilder();
        //        for (int i = 0; i < bytes.Length; i++)
        //        {
        //            builder.Append(bytes[i].ToString("x2"));
        //        }
        //        return builder.ToString();
        //    }
        //}
        //  private string connectionString = "Data Source=USER-PC\\ROPAH;Initial Catalog=Doctor24;Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";


        [HttpGet]
        public ActionResult SearchDoc()
        {
            //string selectSQL;
            //selectSQL = "SELECT First"
            return View();
        }
        public List<MedicalAidAccepted> GetMedicalAid()
        {
            List<MedicalAidAccepted> MedicalAidSchemes = db.MedicalAidAccepteds.ToList();
            return MedicalAidSchemes;
        }

        [HttpPost]
        public ActionResult SearchDoc(string Ailment, string PaymentMethod, string MedicalScheme)
        {
            List<PATIENT> patients = new List<PATIENT>();
            return View(patients);
        }

        [HttpGet]

        public ActionResult AboutUs()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Help()
        {
            return View();
        }
        [HttpGet]
        public ActionResult FAQs()
        {
            return View();

        }

    }


}


