using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DEL2.Models;
using DEL2.ViewModels;

namespace DEL2.Controllers
{
    public class DataController : Controller
    {
        Doctor24Entities2 db = new Doctor24Entities2();
        // GET: Data
        [HttpGet]
        public ActionResult Index()
        {
            return View(db.PATIENTs.ToList());
        }
        [HttpGet]
        public ActionResult PatRegistration()
        {
            return View();
        }
        [HttpPost]
        public ActionResult PatientRegistration(string name, string surname, string IDNumber, string cellphone, string email, string password, string confirmpassword, string SICKNESS)
        {
            PATIENT newPatient = new PATIENT();
            newPatient.Name = name;
            newPatient.Surname = surname;
            newPatient.ID = IDNumber;
            newPatient.ContactNumber = cellphone;
            newPatient.EmailAddress = email;
            //Do we add password and confimpassword here and in the database//
            db.PATIENTs.Add(newPatient);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult UpdatePatient()
        {
            return View();
        }
        [HttpPost]
        public ActionResult PatientUpdate(int id)
        {
            var patient = db.PATIENTs.Where(x => x.PatientID == id).FirstOrDefault();
            return View(patient);
        }

    }
}