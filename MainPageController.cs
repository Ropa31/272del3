﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using DEL2.Models;

namespace DEL2.Controllers
{

    public class MainPageController : Controller
    {
      
        SqlConnection myConnection = new SqlConnection("Data Source=USER-PC\\ROPA;Initial Catalog=Doctor24;Integrated Security=True");
        // GET: MainPage
        [HttpGet]
        public ActionResult HOME()
        {
            return View();
        }
       [HttpGet]
       public ActionResult DOCTOR()
        {
            return View();
        }
        [HttpGet]
        public ActionResult APPOINTMENT()
        {
            return View();
        }
        [HttpGet]
        public ActionResult PatientsAwaitingConf()
        {
            return View();
        }
        [HttpGet]
        public ActionResult CurrentPat()
        {
            return View();
        }
        [HttpGet]
        public ActionResult DocRegistration()
        {
            return View();
        }
        
       [HttpGet]
        public ActionResult PatRegistration(string name,string surname,int IDNum , int cellno,string email)
        {
            Doctor24Entities db = new Doctor24Entities();
            Patient PatientDetails = new Patient();
            PatientDetails.Name = name;
            PatientDetails.Surname = surname;
            PatientDetails.IDNum = IDNum;
            PatientDetails.CellNum = cellno;
            PatientDetails.Email = email;
            db.Patients.Add(PatientDetails);
            db.SaveChanges();
            return View("SearchDoc");
           
        }
        [HttpGet]
        public ActionResult SearchDoc()
        {
            return View();
        }
        [HttpGet]
            public ActionResult AboutUs()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Help()
        {
            return View();
        }
        [HttpGet]
        public ActionResult FAQs()
        {
            return View();
        }
    }
}