using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using HW11.Models;
using System.Security.Cryptography;
using System.Text;
using HW11.Controllers;
using HW11.ViewModels;

namespace HW11.Controllers
{
    public class Login : Controller
    {
        Doctor24Entities1 DB = new Doctor24Entities1();
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Login1()
        {
            return View();

        }

        [HttpPost]
        public ActionResult Login1(string Username, string Password)
        {
            var hashedPassword = ComputeSha256Hash(Password);
            Models.USER user = DB.USERs.Where(zz => zz.Email == Username && zz.Password == hashedPassword).FirstOrDefault();

            if (user != null)
            {
                UserVM userVME = new UserVM();
                userVME.user = user;
                userVME.RefreshGUID(DB);
                TempData["UserVM"] = userVME;
                return RedirectToAction("Index", "Owner");
            }

            return RedirectToAction("Index", "Home", "Error");
        }
        string ComputeSha256Hash(string rawData)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }


        }
        public ActionResult About()
        {
            ViewBag.Message = "You application descsription page";
            return View();

        }

    }

}