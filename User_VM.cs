using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HW11.Models;
using System.Data.Entity;
using System.Web.Mvc;
using HW11.ViewModels;


namespace HW11.ViewModels
{
    public class UserVM
    {
        public USER user;

        public void RefreshGUID(Doctor24Entities1 db)
        {
            db.Configuration.ProxyCreationEnabled = false;
            user.GUID = Guid.NewGuid().ToString();
            user.GUIDexpiry = DateTime.Now.AddMinutes(30);
            var guids = db.USERs.Where(user => user.GUID == user.GUID).Count();
            if (guids > 0)
                Refresh_GUID(db);
            else
            {
                var u = db.USERs.Where(zz => zz.UserID == user.UserID).FirstOrDefault();
                db.Entry(u).CurrentValues.SetValues(user);
                db.SaveChanges();

            }

        }

        public bool IsLoggedIN(Doctor24Entities1 db)
        {
            db.Configuration.ProxyCreationEnabled = false;

            var guids = db.USERs.Where(usr => usr.GUID == user.GUID && usr.GUIDexpiry > DateTime.Now).Count();
            if (guids > 0)
                return true;
            return false;
        }

        internal void Refresh_GUID(Doctor24Entities1 dB)
        {
            throw new NotImplementedException();
        }

        public bool IsLoggedIN(Doctor24Entities1 db, string userGUID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            user = db.USERs.Where(usr => usr.GUID == userGUID && usr.GUIDexpiry > DateTime.Now).FirstOrDefault();
            if (user != null)
                return true;
            return false;
        }
    }

}
