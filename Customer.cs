using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HW11.Models;

namespace HW11.ViewModels
{
    public class OwnerVM
    {

        public int? id;
        public List<OwnerVM> owners;
        public UserVM userVM;
    }
}